﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Coterie.App.WebApi.Contract.Metrics
{
    public class AppExceptionMessageBinding
    {
        public Guid Id { get; set; }
        public string Device { get; set; }
        public string DeviceId { get; set; }
        public string Os { get; set; }
        public string Release { get; set; }
        public string Message { get; set; }
    }
}
