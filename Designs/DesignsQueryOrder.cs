﻿namespace Coterie.App.WebApi.Contract.Designs
{
    public enum DesignsListOrder
    {
        Common,
        Rating,
        CreateDate,
        Random
    }
}