﻿namespace Coterie.App.WebApi.Contract.Designs
{
    public class StitchDimensions
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int TotalStitches { get; set; }
    }
}