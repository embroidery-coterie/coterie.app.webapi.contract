﻿namespace Coterie.App.WebApi.Contract.Designs
{
    public class GetDesignModel
    {
        public string Subscription { get; set; }
        public string DeviceId { get; set; }
    }
}