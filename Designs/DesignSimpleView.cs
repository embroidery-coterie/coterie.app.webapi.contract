﻿using System;
using System.Collections.Generic;

namespace Coterie.App.WebApi.Contract.Designs
{
    public class DesignSimpleView
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Collection { get; set; }
        public bool IsFree { get; set; }
        public DesignerView Designer { get; set; }
        public string Icon { get; set; }
    }
}