﻿using System;

namespace Coterie.App.WebApi.Contract.Designs
{
    public class DesignerView
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}