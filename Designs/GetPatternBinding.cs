﻿namespace Coterie.App.WebApi.Contract.Designs
{
    public class GetPatternBinding
    {
        public string Receipt { get; set; } 
        public string DeviceId { get; set; }
    }
}